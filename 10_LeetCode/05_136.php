<?php
/**
 * Created by PhpStorm.
 * User: yuanlj
 * Date: 2019/5/16
 * Time: 11:24
 */

//https://laravelacademy.org/post/19458.html
class Solution {

    /**
     * @param Integer[] $nums
     * @return Integer
     */
    function singleNumber($nums) {
        $res = 0;
        foreach($nums as $v){
            $res = $res ^ $v;
        }
        return $res;
    }
}

$nums = [2,2,1];
$res = (new Solution())->singleNumber($nums);
var_dump($res);