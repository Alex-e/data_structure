<?php
/**
 * Created by PhpStorm.
 * User: yuanlj
 * Date: 2019/5/16
 * Time: 17:15
 */

class Solution {
    function uniqueMorseRepresentations($words) {
        $morse = [".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."];
        $replaced = [];
        foreach($words as $word){
            $chars = str_split($word);
            $string = '';
            foreach($chars as $char){
                $string .= $morse[ord($char)-ord('a')];
            }
            $replaced[] = $string;
        }
        return count(array_unique($replaced));
    }
}

$words = ["gin", "zen", "gig", "msg"];
$res = (new Solution())->uniqueMorseRepresentations($words);
var_dump($res);