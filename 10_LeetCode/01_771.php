<?php
// 给定字符串J 代表石头中宝石的类型，和字符串 S代表你拥有的石头。 S 中每个字符代表了一种你拥有的石头的类型，你想知道你拥有的石头中有多少是宝石。
// J 中的字母不重复，J 和 S中的所有字符都是字母。字母区分大小写，因此"a"和"A"是不同类型的石头。
//https://laravelacademy.org/post/19448.html

class Solution
{

    /**
     * @param String $J
     * @param String $S
     * @return Integer
     */
    function numJewelsInStones($J, $S)
    {
        $keys = array_filter(str_split($S), function ($val) use ($J) {
            if (in_array($val, str_split($J))) {
                return $val;
            }
        });
        return count($keys);
    }
}

$J = "aA";
$S = "aAAbbbb";
$res = (new Solution())->numJewelsInStones($J, $S);
var_dump($res);