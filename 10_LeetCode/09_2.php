<?php
/**
 * Definition for a singly-linked list.
 * class ListNode {
 *     public $val = 0;
 *     public $next = null;
 *     function __construct($val) { $this->val = $val; }
 * }
 */


class ListNode
{
    public $val = 0;

    public $next = null;

    function __construct($val)
    {
        $this->val = $val;
    }
}

class SingleList
{
    public $head;

    public function __construct(ListNode $node = null)
    {
        $this->head = $node;
    }

    public function is__empty()
    {
        return $this->head == null;
    }

    public function length()
    {
        $cur = $this->head;
        $count = 0;

        while ($cur->val != null) {
            $count++;
            $cur = $cur->next;
        }

        return $count;
    }

    public function travel()
    {
        $cur = $this->head;
        while ($cur != null) {
            echo $cur->val . "\n";
            $cur = $cur->next;
        }
    }

    public function append($item)
    {
        $node = new ListNode($item);
        if ($this->is__empty()) {
            $this->head = $node;
        } else {
            $cur = $this->head;
            while ($cur->next != null) {
                $cur = $cur->next;
            }
            $cur->next = $node;
        }
    }
}

class Solution
{

    private $res = 0;

    /**
     * @param ListNode $l1
     * @param ListNode $l2
     * @return ListNode
     */
    function addTwoNumbers($l1, $l2)
    {
        $node = new ListNode($this->res + $l1->val + $l2->val);
        if ($this->res = (int)$node->val > 9) {
            $node->val -= 10;
        }

        $node->next = (!$this->res && is_null($l1->next) && is_null($l2->next)) ? null : $this->addTwoNumbers($l1->next, $l2->next);
        return $node;
    }
}