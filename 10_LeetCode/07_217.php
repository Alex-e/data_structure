<?php
/**
 * Created by PhpStorm.
 * User: yuanlj
 * Date: 2019/5/16
 * Time: 16:33
 */

//https://laravelacademy.org/post/19481.html
class Solution {

    /**
     * @param Integer[] $nums
     * @return Boolean
     */
    function containsDuplicate($nums) {
        $res = array_count_values($nums);
        return max($res) > 1 ? true : false;
    }
}

$nums = [1, 2, 3, 1];
$res = (new Solution())->containsDuplicate($nums);
var_dump($res);