<?php
/**
 * Created by PhpStorm.
 * User: yuanlj
 * Date: 2019/5/15
 * Time: 19:25
 */

//https://laravelacademy.org/post/19455.html
class Solution {

    /**
     * @param Integer[] $A
     * @return Integer
     */
    function repeatedNTimes($A) {
        $res = [];
        foreach($A as $k=>$v){
            if(!isset($res[$v])){
                $res[$v] = 1;
            }else{
                $res[$v]+=1;
            }
        }
        return array_search(max($res),$res);
    }

    // function repeatedNTimes($A) {
    //     $countValues = array_count_values($A);
    //     $N = count($A)/2;
    //     return array_search($N,$countValues);
    // }
}

$A = [1,2,3,3];
$res = (new Solution())->repeatedNTimes($A);
var_dump($res);