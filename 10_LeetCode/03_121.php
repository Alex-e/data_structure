<?php
// https://laravelacademy.org/post/19454.html

class Solution {

    /**
     * @param Integer[] $prices
     * @return Integer
     */
    function maxProfit($prices) {
        // $income = [];
        $income = 0;
        foreach($prices as $k=>$v){
            if(isset($prices[$k+1]) && $prices[$k+1] > $prices[$k]){
                // $income[]=($prices[$k+1] - $prices[$k]);
                $income+=($prices[$k+1] - $prices[$k]);
            }
        }
        // return array_sum($income);
        return $income;
    }

    /**
     * @param Integer[] $prices
     * @return Integer
     */
    /*function maxProfit($prices) {
        $count = count($prices);
        $income = 0;
        for($i=0;$i<$count;$i++){
            if(isset($prices[$i+1]) && $prices[$i+1] > $prices[$i]){
                $income+=$prices[$i+1] - $prices[$i];
            }
        }
        return $income;
    }*/
}

$prices = [7,1,5,3,6,4];
$res = (new Solution())->maxProfit($prices);
var_dump($res);