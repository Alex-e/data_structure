<?php

class Solution
{
    /**
     * @param String $s
     * @return Integer
     */
    function lengthOfLongestSubstring($s)
    {
        $max = 0;
        $current = '';
        $len = $i = 0;
        for (; $i < strlen($s); $i++) {
            if (strpos($current, $s[$i]) !== false) {
                $current = substr($current, strpos($current, $s[$i]) + 1);
                // var_dump($current);
                $len = strlen($current);
            }
            $current .= $s[$i];
            $len++;
            $max = max($max, $len);
        }

        return $max;
    }
}

$str = "abcabcbb";
// $str = "pwwkew";
// $str = "bbbbb";
// $str = '';
// $str = ' ';
// $str = "dvdf";
$ret = (new Solution())->lengthOfLongestSubstring($str);
var_dump($ret);