<?php
class Solution {

    /**
     * @param String $s
     * @return String
     */
    function longestPalindrome($s) {
        if(strlen($s)<2){
            return $s;
        }
        $max=$s[0];
        for($i=0;$i<strlen($s);$i++){
            for($j=$i+1;$j<strlen($s);++$j){
                $str=substr($s,$i,$j-$i+1);
                $strrev=strrev($str);
                if($str==$strrev && strlen($str)>strlen($max)){
                    $max=$str;
                }
            }
        }
        return $max;
    }
}

$s = 'babad';
$ret = (new Solution())->longestPalindrome($s);
var_dump($ret);