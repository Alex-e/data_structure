<?php
//https://laravelacademy.org/post/19450.html

class Solution
{

    /**
     * @param String[] $emails
     * @return Integer
     */
    function numUniqueEmails($emails)
    {
        $res = [];
        foreach ($emails as $v) {
            $emailExplode = explode('@', $v);
            $name = str_replace('.', '', $emailExplode[0]);
            if (($pos = strpos($name, '+')) !== false) {
                $name = substr($name, 0, $pos);
            }
            $res[] = $name . '@' . $emailExplode[1];
        }
        return count(array_unique($res));
    }
}

// $email = ["test.email+alex@leetcode.com", "test.e.mail+bob.cathy@leetcode.com", "testemail+david@lee.tcode.com"];
// $email = ["a@leetcode.com", "b@leetcode.com", "c@leetcode.com"];
$email = ["test.email+alex@leetcode.com", "test.email@leetcode.com"];
$res = (new Solution())->numUniqueEmails($email);
var_dump($res);