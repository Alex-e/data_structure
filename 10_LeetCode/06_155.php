<?php
/**
 * Created by PhpStorm.
 * User: yuanlj
 * Date: 2019/5/16
 * Time: 11:42
 */

//https://laravelacademy.org/post/19469.html
class MinStack
{

    /**
     * initialize your data structure here.
     */
    function __construct()
    {
        $this->stack = [];
    }

    /**
     * @param Integer $x
     * @return NULL
     */
    function push($x)
    {
        array_unshift($this->stack, $x);
    }

    /**
     * @return NULL
     */
    function pop()
    {
        return array_shift($this->stack);
    }

    /**
     * @return Integer
     */
    function top()
    {
        return $this->stack[0];
    }

    /**
     * @return Integer
     */
    function getMin()
    {
        return min($this->stack);
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * $obj = MinStack();
 * $obj->push($x);
 * $obj->pop();
 * $ret_3 = $obj->top();
 * $ret_4 = $obj->getMin();
 */
$obj = new MinStack();
$obj->push(1);
$obj->push(2);
$pop = $obj->pop();
var_dump($pop);
$pop = $obj->pop();
var_dump($pop);
$obj->push(4);
$obj->push(3);
$top = $obj->top();
var_dump($top);
$min = $obj->getMin();
var_dump($min);




