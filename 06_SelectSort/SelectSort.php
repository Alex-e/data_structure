<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 2018/12/12
 * Time: 10:51 PM
 */

/**
 * 选择排序
 * @param $arr
 */
function select_sort($arr){
    $len = count($arr);
    for($i=0;$i<$len-1;$i++){
        for($j=$i+1;$j<$len;$j++){
            if($arr[$i] > $arr[$j]){
                $tmp = $arr[$i];
                $arr[$i] = $arr[$j];
                $arr[$j] = $tmp;
            }
        }

    }
    return $arr;
}

$arr = [0,3,-1,10,5,20];
$res = select_sort($arr);
print_r($res);