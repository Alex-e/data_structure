
#单链表的节点
class Node():
    def __init__(self,item):
        # 存放数据元素
        self.item = item
        # 指向下一节点的标识
        self.next = None

# 单向链表
class SingleLinkList():

    def __init__(self,node = None):
        self.__head = node

    # 判断链表是否为空
    def is_empty(self):
        return self.__head == None


    # 链表的长度
    def length(self):
        # cur游标，用来移动遍历节点
        cur = self.__head

        # count用来记录节点数量
        count = 0

        while cur != None:
            count += 1
            cur = cur.next
        return count
    
    # 遍历整个链表
    def travel(self):
        cur = self.__head
        while cur != None:
            print(cur.item,end = " ")
            cur = cur.next
        print("")

    # 链表尾部添加元素，尾插法
    def append(self,item):
        node =Node(item)

        if self.is_empty():
            self.__head = node
        else:
            cur = self.__head
            while cur.next != None:
                cur = cur.next
            cur.next = node

    # 链表头部添加元素，头插法
    def add(self,item):
        node = Node(item)
        node.next = self.__head
        self.__head = node

    # 任意指定位置添加元素
    def insert(self,pos,item):
        # 处理特殊情况
        if pos <= 0:
            self.add(item)
        elif pos > self.length():
            self.append(item)
        else:    

            node = Node(item)
            pre = self.__head
            count = 0
            while count < pos-1:
                count+=1
                pre = pre.next

            # while循环结束后，pre就是pos的前一个节点
            node.next = pre.next
            pre.next = node

    # 查找节点是否存在
    def search(self,item):
        cur = self.__head

        while cur != None:
            if cur.item == item:
                return True
            else:
                cur = cur.next

        return False

    # 移除节点
    def remove(self,item):
        cur = self.__head
        pre = None

        while cur != None:
            if cur.item == item:
                # 先检测是不是首节点
                if pre == None:
                    self.__head = cur.next  
                else:
                    pre.next = cur.next
                break
            else:
                pre = cur
                cur = cur.next    



# 测试
if __name__ == "__main__":
    ll = SingleLinkList()
    print(ll.is_empty())
    print(ll.length())

    ll.append(1)
    print(ll.is_empty())
    print(ll.length())

    ll.append(2)
    ll.append(3)
    ll.append(4)
    ll.append(5)

    ll.travel()

    ll.add(0)
    ll.travel()

    ll.insert(-1,7)
    ll.insert(2,8)
    ll.insert(20,9)
    ll.travel()

    ll.remove(9)
    ll.travel()

    ll.remove(7)
    ll.travel()
