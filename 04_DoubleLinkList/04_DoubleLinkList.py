# 节点
class Node(object):
    def __init__(self,item):
        self.item = item
        self.next = None
        self.prev = None


# 双向链表
class DoubliLinkList(object):
    def __init__(self,node = None):
        self.__head = node

    # 判断链表是否为空
    def is_empty(self):
        return self.__head is None


    # 链表的长度
    def length(self):
        # cur游标，用来移动遍历节点
        cur = self.__head

        # count用来记录节点数量
        count = 0

        while cur != None:
            count += 1
            cur = cur.next
        return count
    
    # 遍历整个链表
    def travel(self):
        cur = self.__head
        while cur != None:
            print(cur.item,end = " ")
            cur = cur.next
        print("")

    # 链表尾部添加元素，尾插法
    def append(self,item):
        node =Node(item)

        if self.is_empty():
            self.__head = node
        else:
            cur = self.__head
            while cur.next != None:
                cur = cur.next
            cur.next = node
            node.prev = cur

    # 链表头部添加元素，头插法
    def add(self,item):
        node = Node(item)
        if self.is_empty():
            self.__head = node

        node.next = self.__head
        self.__head.prev = node
        self.__head = node

    # 任意指定位置添加元素
    def insert(self,pos,item):
        # 处理特殊情况
        if pos <= 0:
            self.add(item)
        elif pos > self.length():
            self.append(item)
        else:    

            node = Node(item)
            cur = self.__head
            count = 0
            while count < pos:
                count+=1
                cur = cur.next

            # while循环结束后，cur就是pos的当前位置
            node.next = cur
            node.prev = cur.prev
            cur.prev.next = node
            cur.prev = node

    # 查找节点是否存在
    def search(self,item):
        cur = self.__head

        while cur != None:
            if cur.item == item:
                return True
            else:
                cur = cur.next

        return False

    # 移除节点
    def remove(self,item):
        cur = self.__head

        while cur != None:
            if cur.item == item:
                # 先检测是不是首节点
                if cur == self.__head:
                    self.__head = cur.next
                    # 判断链表是否只有一个节点
                    if cur.next != None:
                        cur.next.prev = None  

                else:
                    cur.prev.next = cur.next
                    # 如果是尾节点，cur.next 等于None
                    if cur.next:
                        cur.next.prev = cur.prev

                break
            else:
                cur = cur.next

if __name__ == "__main__":
    ll = DoubliLinkList()
    print(ll.is_empty())
    print(ll.length())

    ll.append(1)
    print(ll.is_empty())
    print(ll.length())

    ll.append(2)
    ll.append(3)
    ll.append(4)
    ll.append(5)

    ll.travel()

    ll.add(0)
    ll.travel()

    ll.insert(-1,7)
    ll.insert(2,8)
    ll.insert(20,9)
    ll.travel()

    ll.remove(9)
    ll.travel()

    ll.remove(7)
    ll.travel()