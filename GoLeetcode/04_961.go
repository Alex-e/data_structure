package main

import (
	"fmt"
)

func main() {
	A := []int{1, 2, 3, 3}
	res := repeatedNTimes(A)
	fmt.Println(res)
}

func repeatedNTimes(A []int) int {
	res := map[int]int{}
	for _, v := range A {
		_, exists := res[v]
		if !exists {
			res[v] = 1
		} else {
			res[v] += 1
		}
	}
	//fmt.Println(res)
	max := 0
	maxKey := 0
	for k, v := range res {
		if v > max {
			max = v
			maxKey = k
		}
	}
	return maxKey
}
