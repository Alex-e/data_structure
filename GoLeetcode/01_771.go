package main

import (
	"fmt"
	"strings"
)

func main() {
	var J, S = "aA", "aAAbbbb"
	res := numJewelsInStones(J, S)
	fmt.Println(res)
}

func numJewelsInStones(J string, S string) int {
	res := []string{}
	sSplit := strings.Split(S, "")
	jSplit := strings.Split(J, "")
	for _, v := range sSplit {
		for _, v1 := range jSplit {
			if v == v1 {
				res = append(res, v)
			}
		}
	}
	fmt.Println(res)
	return len(res)
}
