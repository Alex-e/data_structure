package main

import (
	"fmt"
)

func main() {
	words := []string{"gin", "zen", "gig", "msg"}
	res := uniqueMorseRepresentations(words)
	fmt.Println(res)
}

func uniqueMorseRepresentations(words []string) int {
	morse := []string{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."}
	res := map[string]int{}
	aNum := []byte("a")[0]

	for _, v := range words {
		charsSlice := []byte(v)
		strings := ""
		for _, v1 := range charsSlice {
			strings += morse[v1-aNum]
		}
		_, exists := res[strings]
		if !exists {
			res[strings] = 1
		}
	}
	//fmt.Println(res)
	return len(res)
}
