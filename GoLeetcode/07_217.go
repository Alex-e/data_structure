package main

import "fmt"

func containsDuplicate(nums []int) bool {
	tmp := map[int]int{}
	for _, v := range nums {
		_, exists := tmp[v]
		if !exists {
			tmp[v] = 1
		} else {
			return true
		}
	}
	return false
}

func main() {
	nums := []int{1, 2, 3, 1}
	res := containsDuplicate(nums)
	fmt.Println(res)

}
