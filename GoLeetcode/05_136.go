package main

import "fmt"

func main() {
	nums := []int{2, 2, 1}
	res := singleNumber(nums)
	fmt.Println(res)
}

func singleNumber(nums []int) int {
	res := 0
	for _, v := range nums {
		res = res ^ v
	}
	return res
}
