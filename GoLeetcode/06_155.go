package main

import (
	"fmt"
)

type MinStack struct {
	Stack []int
}

/** initialize your data structure here. */
func Constructor() MinStack {
	return MinStack{}
}

func (this *MinStack) Push(x int) {
	this.Stack = append(this.Stack, x)
	return
}

func (this *MinStack) Pop() {
	this.Stack = this.Stack[0 : len(this.Stack)-1]
}

func (this *MinStack) Top() int {
	return this.Stack[len(this.Stack)-1]
}

func (this *MinStack) GetMin() int {
	//minVal := this.Stack[0]
	//for _, v1 := range this.Stack {
	//	if v1 < minVal {
	//		minVal = v1
	//	}
	//}
	//return minVal

	min := 0
	for i := 1; i < len(this.Stack); i++ {
		if this.Stack[min] > this.Stack[i] {
			min = i
		}
	}
	return this.Stack[min]
}

/**
 * Your MinStack object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Push(x);
 * obj.Pop();
 * param_3 := obj.Top();
 * param_4 := obj.GetMin();
 */
func main() {
	obj := Constructor()
	obj.Push(-2)
	obj.Push(0)
	obj.Push(-3)

	//min := obj.GetMin()
	//fmt.Println(min)

	obj.Pop()
	//top := obj.Top()
	//fmt.Println(top)

	min := obj.GetMin()
	fmt.Println(min)
}
