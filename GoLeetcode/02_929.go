package main

import (
	"fmt"
)

func main() {
	//emails := []string{"test.email+alex@leetcode.com", "test.e.mail+bob.cathy@leetcode.com", "testemail+david@lee.tcode.com"}
	//emails := []string{"testemail@leetcode.com", "testemail1@leetcode.com", "testemail+david@lee.tcode.com"}
	emails := []string{"test.email+alex@leetcode.com", "test.email.leet+alex@code.com"}
	res := numUniqueEmails(emails)
	fmt.Println(res)
}

/*func numUniqueEmails(emails []string) int {
	mapStr := map[string]int{}
	for _, v := range emails {
		emailsExplode := strings.Split(v, "@")
		emailsExplode[0] = strings.Replace(emailsExplode[0], ".", "", -1)

		pos := strings.Index(emailsExplode[0], "+")
		str := ""
		if (pos != -1) {
			str = emailsExplode[0][0:pos]
		} else {
			str = emailsExplode[0]
		}
		mapStr[str+"@"+emailsExplode[1]] = 1
	}
	return len(mapStr)
}*/

func numUniqueEmails(emails []string) int {
	emailList := make([]string, 0)
	for _, v := range emails {
		meetAt := false
		meetPlus := false
		emailAddr := ""
		for _, ev := range v {
			//fmt.Println("ev: ", ev, string(ev))
			if meetAt {
				//fmt.Println("meet @ ", ev)
				emailAddr += string(ev)
			} else {
				if ev == 64 {
					emailAddr += string(ev)
					meetAt = true
					continue
				}
				if meetPlus {
					continue
				} else {
					if ev == 43 {
						meetPlus = true
						continue
					}
					if ev == 46 {
						continue
					}
					emailAddr += string(ev)
				}
			}
		}

		if len(emailList) == 0 {
			emailList = append(emailList, emailAddr)
		} else {
			exist := false
			for _, em := range emailList {
				if em == emailAddr {
					//fmt.Println("em: ", em, " emailAddr: ", emailAddr)
					exist = true
				}
			}
			if !exist {
				emailList = append(emailList, emailAddr)
			}
		}
	}
	//fmt.Println(emailList)
	return len(emailList)
}
