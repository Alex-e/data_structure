package main

import "fmt"

func main() {
	prices := []int{7, 1, 5, 3, 6, 4}
	res := maxProfit(prices)
	fmt.Println(res)
}

func maxProfit(prices []int) int {
	res := 0
	count := len(prices)

	for i := 0; i < count; i++ {
		if i < count-1 && prices[i+1] > prices[i] {
			res += (prices[i+1] - prices[i])
		}
	}
	return res
}
