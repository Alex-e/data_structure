<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 2018/12/13
 * Time: 9:03 PM
 */

function insert_sort($arr)
{

    $len = count($arr);
    for ($i = 1; $i < $len; $i++) {
        $insertVal = $arr[$i];
        $insertIndex = $i - 1;

        while ($insertIndex >= 0 && $insertVal < $arr[$insertIndex]) {
            $arr[$insertIndex + 1] = $arr[$insertIndex];
            $insertIndex--;
        }
        $arr[$insertIndex + 1] = $insertVal;
    }
    return $arr;
}

$arr = [0, 5, -1, 20, 13, 15];
$res = insert_sort($arr);
print_r($res);


function insertOrder($data){
    for ($i=0;$i<count($data)-1;$i++){
        $temp=$data[$i];
        for($j=$i-1;$j>=0;$j--){
            if($data[$j]>$temp){
                $data[$j+1]=$data[$j];
            }else{
                break;
            }
        }
        $data[$j+1]=$temp;//调整比较数
    }
    return $data;
}
$arr = [0, 5, -1, 20, 13, 15];
$res = insertOrder($arr);
print_r($res);


