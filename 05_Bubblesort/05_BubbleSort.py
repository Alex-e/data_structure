#coding:utf-8

# 冒泡排序
def bubble_sort(alist):
    count = len(alist)
    for i in range(0,count-1):
        for j in range(0,count-1-i):
            if alist[j] > alist[j+1]:
                alist[j],alist[j+1] = alist[j+1],alist[j]
    return alist        
alist = [0,3,-1,10,5,20]
alist = bubble_sort(alist)

print(alist)