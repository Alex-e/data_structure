<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 2018/12/12
 * Time: 10:14 PM
 */

/**
 * 冒泡排序
 * @param $arr
 * @return mixed
 */
function bubble_sort($arr){
    $len = count($arr);

    for($i=0;$i<$len-1;$i++){
        for($j=0;$j<$len-1-$i;$j++){
            if($arr[$j] > $arr[$j+1]){
                $tmp = $arr[$j];
                $arr[$j] = $arr[$j+1];
                $arr[$j+1] = $tmp;
            }
        }
    }
    return $arr;
}

$arr = [0,3,-1,10,5,20];

$res = bubble_sort($arr);

print_r($res);